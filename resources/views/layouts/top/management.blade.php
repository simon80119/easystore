<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="">EasyStore</a>
</div>
<ul class="nav navbar-right top-nav">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
        <ul class="dropdown-menu message-dropdown">
            @foreach($messages as $message)
            <li class="message-preview">
                <a href="#">
                    <div class="media">
                        <div class="media-body">
                            <h5 class="media-heading"><strong>{{$message->user_name}}</strong>
                            </h5>
                            <p class="small text-muted"><i class="fa fa-clock-o"></i> {{$message->created_at}}</p>
                            <p>{{str_limit($message->content,20)}}...</p>
                        </div>
                    </div>
                </a>
            </li>
            @endforeach
            <li class="message-footer">
                <a href="#">查看所有訊息</a>
            </li>
        </ul>
    </li>
    {{--<li class="dropdown">--}}
        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>--}}
        {{--<ul class="dropdown-menu alert-dropdown">--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>--}}
            {{--</li>--}}
            {{--<li class="divider"></li>--}}
            {{--<li>--}}
                {{--<a href="#">View All</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{$store->email}} <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="#"><i class="fa fa-fw fa-user"></i> 個人檔案</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-fw fa-gear"></i> 設定</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="{{route('management::logout')}}"><i class="fa fa-fw fa-power-off"></i>登出</a>
            </li>
        </ul>
    </li>
</ul>
