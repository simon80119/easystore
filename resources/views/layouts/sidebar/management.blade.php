<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li>
            <a href="{{route('management::index')}}"><i class="fa fa-fw fa-dashboard"></i> 首頁</a>
        </li>
        <li>
            <a href="{{route('management::store_set')}}"><i class="fa fa-fw fa-desktop"></i> 店家設定</a>
        </li>
        <li>
            <a href=""><i class="fa fa-fw fa-table"></i> 商品管理</a>
        </li>
        <li>
            <a href=""><i class="fa fa-fw fa-edit"></i> 選單管理</a>
        </li>
        <li>
            <a href=""><i class="fa fa-fw fa-arrows-v"></i> 分類管理 </a>
        </li>
        <li>
            <a href=""><i class="fa fa-fw fa-file"></i> 公告管理</a>
        </li>
        <li>
            <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> 帳號設定</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->