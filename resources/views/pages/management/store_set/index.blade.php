@extends('layouts.management')
@section('title','店家設定')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            店家設定
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('management::index')}}">店家管理</a>
            </li>
            <li class="active">
                <i class="fa fa-edit"></i> 店家設定
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <form role="form">
            <div class="form-group">
                <label>店家名字</label>
                <input class="form-control">
                <p class="help-block"></p>
            </div>
            <div class="form-group">
                <label>店家背景</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家Banner</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家關於我</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家電話</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家手機</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家地址</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="form-group">
                <label>店家狀態</label>
                <input class="form-control">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <button type="submit" class="btn btn-default">送出</button>
            <button type="reset" class="btn btn-default">重填</button>

        </form>

    </div>
</div>
<!-- /.row -->
@endsection