<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect(route('management::index'));
});
//店家後台
Route::group(['as' =>'management::', 'prefix' => 'management','namespace' => 'management'], function () {
    //未登入
    Route::get('login', 'AuthController@getLogin')->name('login');
    Route::post('login', 'AuthController@postLogin');
    Route::get('register', 'AuthController@getRegister')->name('register');
    Route::post('register', 'AuthController@postRegister');
    //已登入
    Route::group(['middleware' => 'auth:management'], function () {
        //首頁
        Route::any('/', 'HomeController@index')->name('index');
        Route::any('/store_set', 'StoreController@index')->name('store_set');

        //登出
        Route::get('/logout', 'AuthController@logout')->name('logout');
    });
});
