<?php

namespace App\Http\Controllers\management;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\management\HomeController;

class StoreController extends HomeController
{
    //
    public function index()
    {
        return \View::make('pages.management.store_set.index');
    }
}
