<?php

namespace App\Http\Controllers\management;

use App\StoreMessage;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Store;

class HomeController extends Controller
{
    public function __construct()
    {
        $store = \Auth::guard('management')->user();
        $messages = $store->messages()->where('status' ,StoreMessage::STATUS_UNREADED)->take(5)->get();
        \View::share('store',$store);
        \View::share('messages',$messages);
    }
    public function index()
    {
        return view('pages.management.home.index');
    }
}
