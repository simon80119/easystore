<?php

use Illuminate\Database\Seeder;

class StoreMessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('store_messages')->insert([
            'store_id' => '1',
            'user_name' => str_random(10),
            'user_email' => str_random(10).'@gmail.com',
            'user_mobile_number' => str_random(10),
            'content' => str_random(10),
            'status' => '0',
        ]);
    }
}
