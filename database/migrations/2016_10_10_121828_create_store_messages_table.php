<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('store_id',20);
            $table->string('user_name',20);
            $table->string('user_email',20);
            $table->string('user_mobile_number',20);
            $table->text('content');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_messages');
    }
}
