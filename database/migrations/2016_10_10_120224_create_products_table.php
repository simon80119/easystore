<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('store_id',20);
            $table->string('product_img');
            $table->string('product_name',20);
            $table->integer('product_price');
            $table->string('product_introduce');
            $table->string('status',1)->default(0);    //商品狀態(0->下架   1->上架)
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('products');
    }
}
