<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url',20)->unique();
            $table->string('mail')->unique();
            $table->string('password');
            $table->string('store_background')->nullable();
            $table->string('store_name',20)->nullable();
            $table->string('store_banner')->nullable();
            $table->text('store_about')->nullable();   //店家關於我  顯示在首頁
            $table->string('store_tel',20)->nullable();
            $table->string('store_mobile_number',20)->nullable();
            $table->string('store_address',20)->nullable();
            $table->string('status',1)->default(0);    //店家狀態(0->下線   1->上線)
            $table->rememberToken();
            $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('stores');
    }
}
